Options nodate nonumber linesize = 64;
*******************************************************************************;                                                                
* MASNA Applied Linear Models I                                                ;                                                                
* Homework 2 Answers - SAS code                                                ;
*                                                          	                   ;
*                                                                              ;                                                                
*******************************************************************************; 
Problem 1;
data problem_1; *** BASS CHARACTERISTICS  **;
title;
input TL  WT ;
datalines;
 387     720                                
 366     680                                
 421    1060                                
 329     480                                
 293     330                                
 273     270                                
 268     220                                
 294     380                                
 198     108                                
 185      89                                
 169      68                                
 102      28                                
 376     764                                
 375     864                                
 374     718                                
 349     648                                
 412    1110                                
 268     244                                
 243     180                                
 191      84                                
 204     108                                
 183      72                                
;
*a) Use a regression model to see how well weight can be estimated by length;
proc reg; 
model wt = tl;
run;

*** The null hypothesis is that there is no connection between weight and length. 
*** The alternative hypothesis is that there is a connection
*** The p-value is <0.0001 so we reject the null hypothesis in favour of the alternative hypothesis at any reasonable significance level
*** Conclusion: weight CAN be estimated by length ;

*b) Use the residuals to check for violation of assumptions;
proc reg plots(only)=rstudentbypredicted;
model wt = tl;
output out=RegOut P=Predicted RSTUDENT=RStudent r=resid;
RUN;

proc sgplot data=RegOut;
scatter x=tl y=RStudent;
run;

*** The graph of residuals against predicted values is not evenly scattered. Ie there is a clear pattern. 
*** Hence we have heteroscedasticity!
*** Residuals form a parabola, suggesting that the model is wrong, rather than non-constant variance.
*** Below I drew a scatterplot of the data: ;

proc sgplot data=problem_1;
    scatter x = TL  y = WT;
run;

*** Looking at the scatterplot we can clearly see that we have non-linear data (either polynomial or exponential)

*c) Calculate a 95% confidence interval on the mean weight of a 300-mm bass;



PROC means data=problem_1 STD;

/*For confidence limits of mean values in the sample, SAS has the clm option.
It provides confidence intervals for a mean predicted value of each observation.  
For individual predicted values for each observation, 
confidence limits are produced using the cli option (must be used without clm)*/
proc reg
model wt = tl / clm;
run;
* Of course, if the value is not in the sample (as in our case), we compute by hand - see the handout. -:;

Problem 2;
data problem_2; ***student exam scores***;
input student  MIDTERM    FINAL ;
datalines;
  1       82        76
  2       73        83
  3       95        89
  4       66        76
  5       84        79
  6       89        73
  7       51        62
  8       82        89
  9       75        77
 10       90        85
 11       60        48
 12       81        69
 13       34        51
 14       49        25
 15       87        74
run;
*a.	Do the regression analysis to predict the final average based on the midterm examination score. Evaluate the results.;
proc reg data=problem_2;
model final=midterm;
run;

*** The prediction is that the final result = 14.40 + 0.77*midterm result
*** Looking at the residuals plot we see that the residuals are much greater for lower values than for higher test values.
*** Hence we have heteroscedasticity (decreasing variance)

* b.	Estimate, using a 90% confidence interval, the value of a midterm score for a student whose final average will be 70.;
* Keep calm and check the handout on pp. 20-21; we have to do this by hand;

*** see excel insert

proc reg data=problem_2;
model final=midterm;
run;

* c.	Fit a regression through the origin and compare it with part (a).  Which model seems best?;

proc reg data=problem_2;
model midterm=final / noint;
run;

*** The model with the intercept at the origin has a higher R^2 - 0.98 as compared to 0.60, suggesting that it is a worse fit


Problem 3;
data problem_3; ***prices***;
input Year  Energy              Transp;
datalines;
60        22.4                29.8
61        22.5                30.1
62        22.6                30.8
63        22.6                30.9
64        22.5                31.4
65        22.9                31.9
66        23.3                32.3
67        23.8                33.3
68        24.2                34.3
69        24.8                35.7
70        25.5                37.5
71        26.5                39.5
72        27.2                39.9
73        29.4                41.2
74        38.1                45.8
75        42.1                50.1
76        45.1                55.1
77        49.4                59.0
78        52.5                61.7
79        65.7                70.5
80        86.0                83.1
81        97.7                93.2
82        99.2                97.0
83        99.9                99.3
84        100.9                103.7
85        101.6                106.4
86        88.2                102.3
87        88.6                105.4
88        89.3                108.7
89        94.3                114.1
90        102.1                120.5
91        102.5                123.8
92        103.0                126.5
93        104.2                130.4
94        104.6                134.3
;
*a.	Perform a correlation analysis to determine the relationship between transportation and energy.  
*Calculate the confidence interval on the correlation coefficient.  Explain the results.;
*Answer: conf. interval on correlation is given by the fisher option;
proc corr data=problem_3 data=a fisher ( biasadj=no ); 
var energy transp;
run;
*b.	Perform separate regression analyses using year as the independent variable and transportation and energy as dependent variables.;  
*Use residual plots to check assumptions.  Explain the results.;
/*Code for this is given above, you just run two separate regressions.*/
proc reg data=problem_3;
model transp=year;
run;
proc reg plots(only)=rstudentbypredicted;
model transp=year;
output out=P3_Mod1 P=Predicted RSTUDENT=RStudent r=resid;
RUN;
proc sgplot data=P3_Mod1;
scatter x=year y=RStudent;
run;

*************************************************;
*************************************************;
*For the next two problems, code is identical to the code provided above;
*So I am skipping it for now - you can copy and paste;
*************************************************;
*************************************************;

data problem_4; ***Politicians keeping promises***;
input polit  MADE    KEPT;
datalines;
  1     21       7
  2     40       5
  3     31       6
  4     62       1
  5     28       5
  6     50       3
  7     55       2
  8     43       6
  9     61       3
 10     30       5
;

data problem_5; *** teen births***;
input OBS    STATE  $  TEEN    MORT;
datalines;
  1     AL      17.4    13.3
  2     AR      19.0    10.3
  3     AZ      13.8     9.4
  4     CA      10.9     8.9
  5     CO      10.2     8.6
  6     CT       8.8     9.1
  7     DE      13.2    11.5
  8     FL      13.8    11.0
  9     GA      17.0    12.5
 10     IA       9.2     8.5
 11     ID      10.8    11.3
 12     IL      12.5    12.1
 13     IN      14.0    11.3
 14     KS      11.5     8.9
 15     KY      17.4     9.8
 16     LA      16.8    11.9
 17     MA       8.3     8.5
 18     MD      11.7    11.7
 19     ME      11.6     8.8
 20     MI      12.3    11.4
 21     MN       7.3     9.2
 22     MO      13.4    10.7
 23     MS      20.5    12.4
 24     MT      10.1     9.6
 25     NB       8.9    10.1
 26     NC      15.9    11.5
 27     ND       8.0     8.4
 28     NH       7.7     9.1
 29     NJ       9.4     9.8
 30     NM      15.3     9.5
 31     NV      11.9     9.1
 32     NY       9.7    10.7
 33     OH      13.3    10.6
 34     OK      15.6    10.4
 35     OR      10.9     9.4
 36     PA      11.3    10.2
 37     RI      10.3     9.4
 38     SC      16.6    13.2
 39     SD       9.7    13.3
 40     TN      17.0    11.0
 41     TX      15.2     9.5
 42     UT       9.3     8.6
 43     VA      12.0    11.1
 44     VT       9.2    10.0
 45     WA      10.4     9.8
 46     WI       9.9     9.2
 47     WV      17.1    10.2
 48     WY      10.7    10.8
;


*Problem 6;
data problem_6;
input  AGE    INCOME;
datalines;
 25     1200
 32     1290
 43     1400
 26     1000
 33     1370
 48     1500
 39     6500
 59     1900
 62     1500
 51     2100
 33     1340
 22     1000
 44     1330
 25     1390
 39     1400
 55     2000
 34     1600
 58     1680
 61     2100
 55     2000
;
*a.	Perform the regression of income on age and plot the residuals.  Do the residuals indicate the presence of an outlier?;
proc reg data=problem_6;
model income = age / r;
output out=temp p=predict r=resid student=StRes;
proc print data=temp;
run;
*b.	Calculate the studentized residuals for the regression.  Do these indicate any problems?;
proc reg data=problem_6;
model income = age;
run;
output out=temp student=StRes;
proc print data=temp;
run;
*c.	Calculate Dffits and Dfbetas. Interpret the results.; 
* "influence" option gives you the dffits and dfbetas - will explain them in class;
ods graphics on;
proc reg data=problem_6
plots(label)=(CooksD RStudentByLeverage DFFITS DFBETAS);
model income = age /influence;
run;
ods graphics off;
*If you’ve found an outliner, re-run the regression without that observation. What did you observe?;
* I've found an observation with income of 6500 to be an outlier (obs 7) - yours might be different;
data problem_6c1;
set problem_6 (obs=6);
run;
data problem_6c2;
set problem_6 (firstobs=8 obs=max);
run;
data problem_6final;
set problem_6c1 problem_6c2;
proc print data=problem_6final;
run;
*Now, run regression without that observation:;
ods graphics on;
proc reg data=problem_6final
plots(label)=(CooksD RStudentByLeverage DFFITS DFBETAS);
model income = age /influence;
run;
ods graphics off;

*Code for solving assignment in Problem 7 is given above in various chunks :);
data problem_7; *City latitude and temperature range;
input CITY $  STATE $  LAT    RANGE;
datalines;
Montgome     AL      32.3     18.6
Tuscon       AZ      32.1     19.7
Bishop       CA      37.4     21.9
Eureka       CA      40.8      5.4
San_Dieg     CA      32.7      9.0
San_Fran     CA      37.6      8.7
Denver       CO      39.8     24.0
Washingt     DC      39.0     24.0
Miami        FL      25.8      8.7
Talahass     FL      30.4     15.9
Tampa        FL      28.0     12.1
Atlanta      GA      33.6     19.8
Boise        ID      43.6     25.3
Moline       IL      41.4     29.4
Ft_wayne     IN      41.0     26.5
Topeka       KS      39.1     27.9
Louisv       KY      38.2     24.2
New_Orl      LA      30.0     16.1
Caribou      ME      46.9     30.1
Portland     ME      43.6     25.8
Alpena       MI      45.1     26.5
St_cloud     MN      45.6     34.0
Jackson      MS      32.3     19.2
St_Louis     MO      38.8     26.3
Billings     MT      45.8     27.7
N_PLatte     NB      41.1     28.3
L_Vegas      NV      36.1     25.2
Albuquer     NM      35.0     24.1
Buffalo      NY      42.9     25.8
NYC          NY      40.6     24.2
C_Hatter     NC      35.3     18.2
Bismark      ND      46.8     34.8
Eugene       OR      44.1     15.3
Charestn     SC      32.9     17.6
Huron        SD      44.4     34.0
Knoxvlle     TN      35.8     22.9
Memphis      TN      35.0     22.9
Amarillo     TX      35.2     23.7
Brownsvl     TX      25.9     13.4
Dallas       TX      32.8     22.3
SLCity       UT      40.8     27.0
Roanoke      VA      37.3     21.6
Seattle      WA      47.4     14.7
Grn_bay      WI      44.5     29.9
Casper       WY      42.9     26.6
;

